<?php
require 'connection.php';
if(!isset($_SESSION['user'])) {
    header("Location: login.php");
}
$id = $_SESSION['user']['id'];
$products_sql = "select product_id,service,image,description title,price,quantity from _cart inner join products on `_cart`.product_id=products.id and `_cart`.service='products' and `_cart`.user_id=$id";
$mealplan_sql = "select product_id,service,image,category title,price,quantity from _cart inner join mealplans on `_cart`.product_id=mealplans.id and `_cart`.service='mealplans' and `_cart`.user_id=$id";
$sql = "$products_sql UNION $mealplan_sql";
$result = mysqli_query($conn, $sql);
if(isset($_GET['action']) && $_GET['action']=="remove" && isset($_GET['service']) && isset($_GET['id'])) {
    $sql = "DELETE FROM `_cart` WHERE service='".$_GET['service']."' and user_id=$id and product_id='".$_GET['id']."'";
    mysqli_query($conn, $sql);
    header("Location: cart.php");
}
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <!-- Basic -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">

    <!-- Mobile Metas -->
    <meta name="viewport" content="width=device-width, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no">

    <!-- Site Metas -->
    <title>Zeeliciousfoods | Easy recipes, food reviews, kitchen tips and tricks and health benefits of foods</title>
    <meta name="keywords" content="">
    <meta name="description" content="">
    <meta name="author" content="">

    <!-- Site Icons -->
    <link rel="shortcut icon" href="images/favicon.png"/>
    <link rel="apple-touch-icon" href="images/favicon.png">

    <link rel="stylesheet" href="css/cstyle.css">
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <!-- Site CSS -->
    <link rel="stylesheet" href="css/style.css">
    <!-- Responsive CSS -->
    <link rel="stylesheet" href="css/responsive.css">
    <!-- Custom CSS -->
    <link rel="stylesheet" href="css/custom.css">

    <!-- Modernizer for Portfolio -->
    <script src="js/modernizer.js"></script>

    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>
<body>

<!-- LOADER -->
<div id="preloader">
    <div class="loader">
        <div class="loader__bar"></div>
        <div class="loader__bar"></div>
        <div class="loader__bar"></div>
        <div class="loader__bar"></div>
        <div class="loader__bar"></div>
        <div class="loader__ball"></div>
    </div>
</div><!-- end loader -->
<!-- END LOADER -->

<?php include 'template/header.php'; ?>
<hr>
<div class="container mb-4">
    <div class="row">
        <?php if (mysqli_num_rows($result)) { ?>
        <div class="col-12">
            <div class="table-responsive">
                <table class="table table-striped">
                    <thead>
                    <tr>
                        <th scope="col"></th>
                        <th scope="col" style="color:black">Product</th>
                        <th scope="col" style="color:black">Available</th>
                        <th scope="col" style="color:black" class="text-center">Quantity</th>
                        <th scope="col" style="color:black" class="text-right">Price</th>
                        <th scope="col" style="color:black" class="text-right">Total</th>
                        <th></th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php
                        while ($row = mysqli_fetch_assoc($result)) {
                            $path = $row['image'];
                            $desc = $row['title'];
                            $price = $row['price'];
                            $quantity = $row['quantity'];
                            echo '
                                    <tr>
                                        <td><img src="' . $path . '" width="200" height="100"/> </td>
                                        <td style="color:black">' . $row['title'] . '</td>
                                        <td><input class="form-control" type="number" value="' . $quantity . '" style="width:80px;color:black"/></td>
                                        <td style="color:black" class="text-right">₦' . $row['price'] . '</td>
                                        <td style="color:black" class="text-right">₦'. $quantity*$price .'</td>
                                        <td style="color:black" class="text-right">
                                        <a href="cart.php?action=remove&service='.$row["service"].'&id='.$row["product_id"].'" class="btn btn-sm btn-danger">
                                        <i class="fa fa-trash"></i> </a> </td>
                                    </tr>';
                        }
                    ?>
                    <tr>
                        <td><input class="code1" type="number" placeholder="Coupon code"></td>
                        <td>
                            <button class="button" style="vertical-align:middle"><span>Apply Coupon</span></button>
                        </td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td>
                            <button class="button-update" type="submit" style="vertical-align:middle;color:black">Update
                                cart
                            </button>
                        </td>
                        <td></td>
                    </tr>
                    </tbody>
                </table>
            </div>
        </div>
        <div class="cart_total">
            <h2>Cart totals</h2>
            <div>
                <table class="inner_cart_total">
                    <tbody>
                    <tr>
                        <th>Subtotal</th>
                        <td>
                            ₦124,900
                        </td>
                    </tr>
                    <tr>
                        <th>Total</th>
                        <td>
                            ₦124,900
                        </td>
                    </tr>
                    </tbody>
                </table>
                <div>

                    <div class="checkout">
                        <br><br>
                        <button class="button2" style="vertical-align:middle;height:55px">
                            <span>Proceed to checkout</span></button>
                    </div>

                </div>

            </div>

        </div>
        <?php } else { echo '<h1 style="text-align: center">Cart is Empty.<br><a href="services.php">Continue Shopping</a></h1>'; }?>
    </div>
    </div>
</div>
<hr>
<?php include 'template/footer.php'; ?>

<a href="#" id="scroll-to-top" class="dmtop global-radius"><i class="fa fa-angle-up"></i></a>

<!-- ALL JS FILES -->
<script src="js/all.js"></script>
<!-- ALL PLUGINS -->
<script src="js/custom.js"></script>
<script src="js/portfolio.js"></script>
<script src="js/hoverdir.js"></script>

</body>
</html>